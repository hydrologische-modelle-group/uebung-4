% Calculation of harmonic mean of a one dimensional dataseries.
%
%   Syntax: out = MeanHarmonic_loesung(in1)
%
% Variables:
% in1:   One dimensional vector of input data.
% out:   Geometric mean of input data.

function out = MeanHarmonic_loesung(in1)

n = numel(in1);
out = n/sum(1./in1);