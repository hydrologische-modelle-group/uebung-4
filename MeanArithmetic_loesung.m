% Calculation of arithmetic mean of a one dimensional dataseries.
%
%   Syntax: out = MeanArithmetic_loesung(in1)
%
% Variables:
% in1:   One dimensional vector of input data.
% out:   Arithmetic mean of input data.

function out = MeanArithmetic_loesung(in1)

n = numel(in1);
out = sum(in1)/n;